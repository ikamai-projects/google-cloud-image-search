#!/bin/bash
for i in {1..10}
do
  gcloud app create --region=us-central --project tiara-risk-$i
  gcloud app deploy app.yaml --project tiara-risk-$i --quiet

 echo $i
done
