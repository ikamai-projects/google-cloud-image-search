<?php
$app->get('/', 'App\Controller\BlogController:home');
$app->get('/v2/search', 'App\Controller\GoogleImgControllerV2:search');
$app->get('/search', 'App\Controller\GoogleImgController:search');
