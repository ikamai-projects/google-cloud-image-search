<?php
namespace App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

final class GoogleImgController {

  private $logger;

  public function __construct($logger) {
    $this->logger = $logger;
  }

  public function search(Request $request, Response $response){

    $dt = $this->grab();

    if(isset($_GET['limit'])){
      $lim = $_GET['limit'];
    }else{
      $lim = 30;
    }

    /*
      Inject New Variable
    */
    $mt = array_reverse($dt);
    $i = 0; $pc = [];
    foreach($dt as $w){
    	if($i<$lim){

        $nw = strtolower(str_replace(' On Pinterest','',implode(' ', array_unique(explode(' ',$mt[$i]['title'])))));
        $nw = str_replace('pinterest','',$nw);
        $nw = ucwords(str_replace(' on ',' ',$nw));

        $tl = strtolower(implode(' ', array_unique(explode(' ',$w['title']))));
        $tl = str_replace(' on ',' ',$tl);
    		$ntt = ucwords($tl.' on '.$nw);

        $w['title2'] = $w['title'];
        $w['title'] = $ntt;
        $w['path'] = str_replace(basename($w['path']),$this->x_slug($ntt).'.'.$w['ext'],$w['path']);
        //$w['slug'] = str_replace(basename($w['slug']),$this->x_slug($ntt),$w['slug']);

        //edit
        $pc[] = $w;

    	}
    $i++;}
    $dt = $pc;
    //exe export
  	if(isset($_GET['debug'])){

  	  	$i = 1;
        $dc = [];
  	  	foreach($dt as $d){
  			if($i<=$lim){
  	  		$d = (object) $d;
  	      	echo '<div style="float:left;margin:0 10px 10px 0;border:solid 1px #ececec;width:220px;height:300px;padding:5px;font-size:12px">'.
  	  			'<a href="'.$d->src.'" target="_blank"><img src="'.$d->thb.'" width="220" height="160" /></a><br/>'.
            $i.'. - '.$d->width.'x'.$d->height.' - '.$d->title.
  	  		'</div>';
          $dc[] = $d;
  	  	}
  	  	$i++;}
  	  	echo '<div style="flaot:left;width:100%;display:block;clear:both;"></div><pre>';
        echo '<pre>';
  	  	print_r($dc);

  	}else{

      $i = 1;
      $dat = [];
      foreach($dt as $c){
      	$c = (object) $c;
        if($i<=$lim){
          $dat[] = $c;
        }
      $i++;}

      $pr = sha1($_GET['q']);
      $p1 = substr($pr,0,6);
      $perma = $p1.'/'.$this->x_slug($_GET['q']);

  		$data = [
  			'title' => $this->x_slug($_GET['q']),
        'permalink' => $perma,
        'total' => count($dat),
        'update' => date('Y-m-d H:i:s'),
        'data'  => $dat
  		];

      //export
      $response = $response
        ->withAddedHeader('Access-Control-Allow-Methods','POST, GET, OPTIONS')
        ->withAddedHeader('Access-Control-Allow-Origin','*');
      $r = $response->withJson($data);
      return $r;

  	}


  	exit;
    //$this->logger->addInfo("Something interesting happened");

  }
  public function grab($unit=0){
    $headers = "accept: */*\r\n" .
      "User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0";
    $context = [
      'http' => [
        'method' => 'GET',
        'header' => $headers,
        //'content' => http_build_query($fields),
      ]
    ];
    $context = stream_context_create($context);

  	if($unit==0){
      $r = file_get_contents('https://www.google.com/search?q='.str_replace(array(' ','-'),'+',$_GET['q']).'&dcr=0&tbm=isch&source=lnt&sa=X&ved=&biw=1920&bih=953&dpr=1', false, $context);
    }else{
      $r = file_get_contents('https://www.google.com/search?q=-site:pinterest.com+'.str_replace('-','+',$_GET['q']).'&dcr=0&tbm=isch&source=lnt&sa=X&ved=&biw=1920&bih=953&dpr=1', false, $context);
    }

    $domknt = new \domDocument();
  	@$domknt->loadHTML($r);
  	$dodknt = $domknt->getElementsByTagName('div');
  	$dt = [];

    //giling hasil grab
    $u = 0;
  	foreach($dodknt as $c){
  	  $d = $c->getAttribute('class');
  	  if($d=='rg_meta notranslate'){
  	  	$w = json_decode($c->nodeValue);
        if($w->s!=='' && $w->ity!=='' && isset($w->st)){

          $w->st = ucwords(strtolower($w->st));
          $t = ucwords(str_replace('-',' ',$this->x_slug($w->pt)));
          $t = str_replace(' '.$w->st,'',$t);

          //remove domen
          $b = explode('.',$w->st);
          $b = $b[0];
          $t = str_replace($b,'',$t);
          $t = trim($t);

          //remove pinterest
          if($unit==0){
            $t = preg_replace('/[0-9]+/','', $t);
            $t = trim(str_replace('  ',' ',$t));
            if(substr(strtolower($t),0,5)=='best '){
              $t = substr(strtolower($t),5);
              $t = ucwords($t);
            }
          }
          //uid
          $s = sha1($t);
          $sl = substr(sha1(substr(preg_replace('/[^1-9]/','',$s),0,10)),-10);
          $sa = substr(preg_replace('/[^a-z]/','',$s),0,4);
          $slg = $sa.'/'.$sl.'/'.$this->x_slug($t);

          //img path
          $i1 = substr(sha1(substr(preg_replace('/[^1-9]/','',md5($t)),0,1)),-5);
          $i2 = $this->x_slug($t).'.'.$w->ity;
          $path = $i1.'/'.$i2;

          if($b!=='' && strlen($t)>10 && strpos($t,'%')==false){
            $dt[] = [
             'slug' => $slg,
             'path' => $path,
      	  	 'title' => $t,
             'uniq' => $s,
      	  	 'txt' => ucwords(str_replace('-',' ',$this->x_slug($w->s))),
      	  	 'width' => $w->ow,
      	  	 'height' => $w->oh,
      	  	 'src' => $w->ou,
      	  	 'thb' => $w->tu,
      	  	 'domain' => $w->isu,
      	  	 'ext' => $w->ity,
      	  	 //'st' => $w->st,
             'oc' => strtolower($b),
             'update' => date('Y-m-d H:i:s')
      	  	];
          }
        }
  	  }
  	$u++;}
    $dt = $this->uar($dt,'uniq');
    return $dt;
  }

  public function suggest($q){
    $q = str_replace(' ','-',$q);
    $r = json_decode(file_get_contents('https://www.google.com/complete/search?output=search&client=chrome&q='.$q));
    return $r;
  }
  public function x_slug( $title) {
    $context = 'save';
  	$title = $this->remove_accents(strip_tags($title));
  	// Preserve escaped octets.
  	$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
  	// Remove percent signs that are not part of an octet.
  	$title = str_replace('%', '', $title);
  	// Restore octets.
  	$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
  	if ($this->seems_utf8($title)) {
  		$title = $this->utf8_uri_encode($title, 200);
  	}
  	$title = strtolower($title);
  	if ( 'save' == $context ) {
  		// Convert nbsp, ndash and mdash to hyphens
  		$title = str_replace( array( '%c2%a0', '%e2%80%93', '%e2%80%94' ), '-', $title );
  		// Convert nbsp, ndash and mdash HTML entities to hyphens
  		$title = str_replace( array( '&nbsp;', '&#160;', '&ndash;', '&#8211;', '&mdash;', '&#8212;' ), '-', $title );
  		// Convert forward slash to hyphen
  		$title = str_replace( '/', '-', $title );
  		// Strip these characters entirely
  		$title = str_replace( array(
  			// iexcl and iquest
  			'%c2%a1', '%c2%bf',
  			// angle quotes
  			'%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
  			// curly quotes
  			'%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
  			'%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
  			// copy, reg, deg, hellip and trade
  			'%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
  			// acute accents
  			'%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
  			// grave accent, macron, caron
  			'%cc%80', '%cc%84', '%cc%8c',
  		), '', $title );
  		// Convert times to x
  		$title = str_replace( '%c3%97', 'x', $title );
  	}
  	$title = preg_replace('/&.+?;/', '', $title); // kill entities
  	$title = str_replace('.', '-', $title);
  	$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
  	$title = preg_replace('/\s+/', '-', $title);
  	$title = preg_replace('|-+|', '-', $title);
  	$title = trim($title, '-');
  	return $title;
  }
  public function seems_utf8( $str ) {
  	$this->mbstring_binary_safe_encoding();
  	$length = strlen($str);
  	$this->reset_mbstring_encoding();
  	for ($i=0; $i < $length; $i++) {
  		$c = ord($str[$i]);
  		if ($c < 0x80) $n = 0; // 0bbbbbbb
  		elseif (($c & 0xE0) == 0xC0) $n=1; // 110bbbbb
  		elseif (($c & 0xF0) == 0xE0) $n=2; // 1110bbbb
  		elseif (($c & 0xF8) == 0xF0) $n=3; // 11110bbb
  		elseif (($c & 0xFC) == 0xF8) $n=4; // 111110bb
  		elseif (($c & 0xFE) == 0xFC) $n=5; // 1111110b
  		else return false; // Does not match any model
  		for ($j=0; $j<$n; $j++) { // n bytes matching 10bbbbbb follow ?
  			if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
  				return false;
  		}
  	}
  	return true;
  }
  public function utf8_uri_encode( $utf8_string, $length = 0 ) {
  	$unicode = '';
  	$values = array();
  	$num_octets = 1;
  	$unicode_length = 0;
  	$this->mbstring_binary_safe_encoding();
  	$string_length = strlen( $utf8_string );
  	$this->reset_mbstring_encoding();
  	for ($i = 0; $i < $string_length; $i++ ) {
  		$value = ord( $utf8_string[ $i ] );
  		if ( $value < 128 ) {
  			if ( $length && ( $unicode_length >= $length ) )
  				break;
  			$unicode .= chr($value);
  			$unicode_length++;
  		} else {
  			if ( count( $values ) == 0 ) {
  				if ( $value < 224 ) {
  					$num_octets = 2;
  				} elseif ( $value < 240 ) {
  					$num_octets = 3;
  				} else {
  					$num_octets = 4;
  				}
  			}
  			$values[] = $value;
  			if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
  				break;
  			if ( count( $values ) == $num_octets ) {
  				for ( $j = 0; $j < $num_octets; $j++ ) {
  					$unicode .= '%' . dechex( $values[ $j ] );
  				}
  				$unicode_length += $num_octets * 3;
  				$values = array();
  				$num_octets = 1;
  			}
  		}
  	}
  	return $unicode;
  }
  public function remove_accents( $string ) {
  	if ( !preg_match('/[\x80-\xff]/', $string) )
  		return $string;
  	if ($this->seems_utf8($string)) {
  		$chars = array(
  		// Decompositions for Latin-1 Supplement
  		'ª' => 'a', 'º' => 'o',
  		'À' => 'A', 'Á' => 'A',
  		'Â' => 'A', 'Ã' => 'A',
  		'Ä' => 'A', 'Å' => 'A',
  		'Æ' => 'AE','Ç' => 'C',
  		'È' => 'E', 'É' => 'E',
  		'Ê' => 'E', 'Ë' => 'E',
  		'Ì' => 'I', 'Í' => 'I',
  		'Î' => 'I', 'Ï' => 'I',
  		'Ð' => 'D', 'Ñ' => 'N',
  		'Ò' => 'O', 'Ó' => 'O',
  		'Ô' => 'O', 'Õ' => 'O',
  		'Ö' => 'O', 'Ù' => 'U',
  		'Ú' => 'U', 'Û' => 'U',
  		'Ü' => 'U', 'Ý' => 'Y',
  		'Þ' => 'TH','ß' => 's',
  		'à' => 'a', 'á' => 'a',
  		'â' => 'a', 'ã' => 'a',
  		'ä' => 'a', 'å' => 'a',
  		'æ' => 'ae','ç' => 'c',
  		'è' => 'e', 'é' => 'e',
  		'ê' => 'e', 'ë' => 'e',
  		'ì' => 'i', 'í' => 'i',
  		'î' => 'i', 'ï' => 'i',
  		'ð' => 'd', 'ñ' => 'n',
  		'ò' => 'o', 'ó' => 'o',
  		'ô' => 'o', 'õ' => 'o',
  		'ö' => 'o', 'ø' => 'o',
  		'ù' => 'u', 'ú' => 'u',
  		'û' => 'u', 'ü' => 'u',
  		'ý' => 'y', 'þ' => 'th',
  		'ÿ' => 'y', 'Ø' => 'O',
  		// Decompositions for Latin Extended-A
  		'Ā' => 'A', 'ā' => 'a',
  		'Ă' => 'A', 'ă' => 'a',
  		'Ą' => 'A', 'ą' => 'a',
  		'Ć' => 'C', 'ć' => 'c',
  		'Ĉ' => 'C', 'ĉ' => 'c',
  		'Ċ' => 'C', 'ċ' => 'c',
  		'Č' => 'C', 'č' => 'c',
  		'Ď' => 'D', 'ď' => 'd',
  		'Đ' => 'D', 'đ' => 'd',
  		'Ē' => 'E', 'ē' => 'e',
  		'Ĕ' => 'E', 'ĕ' => 'e',
  		'Ė' => 'E', 'ė' => 'e',
  		'Ę' => 'E', 'ę' => 'e',
  		'Ě' => 'E', 'ě' => 'e',
  		'Ĝ' => 'G', 'ĝ' => 'g',
  		'Ğ' => 'G', 'ğ' => 'g',
  		'Ġ' => 'G', 'ġ' => 'g',
  		'Ģ' => 'G', 'ģ' => 'g',
  		'Ĥ' => 'H', 'ĥ' => 'h',
  		'Ħ' => 'H', 'ħ' => 'h',
  		'Ĩ' => 'I', 'ĩ' => 'i',
  		'Ī' => 'I', 'ī' => 'i',
  		'Ĭ' => 'I', 'ĭ' => 'i',
  		'Į' => 'I', 'į' => 'i',
  		'İ' => 'I', 'ı' => 'i',
  		'Ĳ' => 'IJ','ĳ' => 'ij',
  		'Ĵ' => 'J', 'ĵ' => 'j',
  		'Ķ' => 'K', 'ķ' => 'k',
  		'ĸ' => 'k', 'Ĺ' => 'L',
  		'ĺ' => 'l', 'Ļ' => 'L',
  		'ļ' => 'l', 'Ľ' => 'L',
  		'ľ' => 'l', 'Ŀ' => 'L',
  		'ŀ' => 'l', 'Ł' => 'L',
  		'ł' => 'l', 'Ń' => 'N',
  		'ń' => 'n', 'Ņ' => 'N',
  		'ņ' => 'n', 'Ň' => 'N',
  		'ň' => 'n', 'ŉ' => 'n',
  		'Ŋ' => 'N', 'ŋ' => 'n',
  		'Ō' => 'O', 'ō' => 'o',
  		'Ŏ' => 'O', 'ŏ' => 'o',
  		'Ő' => 'O', 'ő' => 'o',
  		'Œ' => 'OE','œ' => 'oe',
  		'Ŕ' => 'R','ŕ' => 'r',
  		'Ŗ' => 'R','ŗ' => 'r',
  		'Ř' => 'R','ř' => 'r',
  		'Ś' => 'S','ś' => 's',
  		'Ŝ' => 'S','ŝ' => 's',
  		'Ş' => 'S','ş' => 's',
  		'Š' => 'S', 'š' => 's',
  		'Ţ' => 'T', 'ţ' => 't',
  		'Ť' => 'T', 'ť' => 't',
  		'Ŧ' => 'T', 'ŧ' => 't',
  		'Ũ' => 'U', 'ũ' => 'u',
  		'Ū' => 'U', 'ū' => 'u',
  		'Ŭ' => 'U', 'ŭ' => 'u',
  		'Ů' => 'U', 'ů' => 'u',
  		'Ű' => 'U', 'ű' => 'u',
  		'Ų' => 'U', 'ų' => 'u',
  		'Ŵ' => 'W', 'ŵ' => 'w',
  		'Ŷ' => 'Y', 'ŷ' => 'y',
  		'Ÿ' => 'Y', 'Ź' => 'Z',
  		'ź' => 'z', 'Ż' => 'Z',
  		'ż' => 'z', 'Ž' => 'Z',
  		'ž' => 'z', 'ſ' => 's',
  		// Decompositions for Latin Extended-B
  		'Ș' => 'S', 'ș' => 's',
  		'Ț' => 'T', 'ț' => 't',
  		// Euro Sign
  		'€' => 'E',
  		// GBP (Pound) Sign
  		'£' => '',
  		// Vowels with diacritic (Vietnamese)
  		// unmarked
  		'Ơ' => 'O', 'ơ' => 'o',
  		'Ư' => 'U', 'ư' => 'u',
  		// grave accent
  		'Ầ' => 'A', 'ầ' => 'a',
  		'Ằ' => 'A', 'ằ' => 'a',
  		'Ề' => 'E', 'ề' => 'e',
  		'Ồ' => 'O', 'ồ' => 'o',
  		'Ờ' => 'O', 'ờ' => 'o',
  		'Ừ' => 'U', 'ừ' => 'u',
  		'Ỳ' => 'Y', 'ỳ' => 'y',
  		// hook
  		'Ả' => 'A', 'ả' => 'a',
  		'Ẩ' => 'A', 'ẩ' => 'a',
  		'Ẳ' => 'A', 'ẳ' => 'a',
  		'Ẻ' => 'E', 'ẻ' => 'e',
  		'Ể' => 'E', 'ể' => 'e',
  		'Ỉ' => 'I', 'ỉ' => 'i',
  		'Ỏ' => 'O', 'ỏ' => 'o',
  		'Ổ' => 'O', 'ổ' => 'o',
  		'Ở' => 'O', 'ở' => 'o',
  		'Ủ' => 'U', 'ủ' => 'u',
  		'Ử' => 'U', 'ử' => 'u',
  		'Ỷ' => 'Y', 'ỷ' => 'y',
  		// tilde
  		'Ẫ' => 'A', 'ẫ' => 'a',
  		'Ẵ' => 'A', 'ẵ' => 'a',
  		'Ẽ' => 'E', 'ẽ' => 'e',
  		'Ễ' => 'E', 'ễ' => 'e',
  		'Ỗ' => 'O', 'ỗ' => 'o',
  		'Ỡ' => 'O', 'ỡ' => 'o',
  		'Ữ' => 'U', 'ữ' => 'u',
  		'Ỹ' => 'Y', 'ỹ' => 'y',
  		// acute accent
  		'Ấ' => 'A', 'ấ' => 'a',
  		'Ắ' => 'A', 'ắ' => 'a',
  		'Ế' => 'E', 'ế' => 'e',
  		'Ố' => 'O', 'ố' => 'o',
  		'Ớ' => 'O', 'ớ' => 'o',
  		'Ứ' => 'U', 'ứ' => 'u',
  		// dot below
  		'Ạ' => 'A', 'ạ' => 'a',
  		'Ậ' => 'A', 'ậ' => 'a',
  		'Ặ' => 'A', 'ặ' => 'a',
  		'Ẹ' => 'E', 'ẹ' => 'e',
  		'Ệ' => 'E', 'ệ' => 'e',
  		'Ị' => 'I', 'ị' => 'i',
  		'Ọ' => 'O', 'ọ' => 'o',
  		'Ộ' => 'O', 'ộ' => 'o',
  		'Ợ' => 'O', 'ợ' => 'o',
  		'Ụ' => 'U', 'ụ' => 'u',
  		'Ự' => 'U', 'ự' => 'u',
  		'Ỵ' => 'Y', 'ỵ' => 'y',
  		// Vowels with diacritic (Chinese, Hanyu Pinyin)
  		'ɑ' => 'a',
  		// macron
  		'Ǖ' => 'U', 'ǖ' => 'u',
  		// acute accent
  		'Ǘ' => 'U', 'ǘ' => 'u',
  		// caron
  		'Ǎ' => 'A', 'ǎ' => 'a',
  		'Ǐ' => 'I', 'ǐ' => 'i',
  		'Ǒ' => 'O', 'ǒ' => 'o',
  		'Ǔ' => 'U', 'ǔ' => 'u',
  		'Ǚ' => 'U', 'ǚ' => 'u',
  		// grave accent
  		'Ǜ' => 'U', 'ǜ' => 'u',
  		);
  		// Used for locale-specific rules
  		$locale = 'en-US';
  		if ( 'de_DE' == $locale || 'de_DE_formal' == $locale || 'de_CH' == $locale || 'de_CH_informal' == $locale ) {
  			$chars[ 'Ä' ] = 'Ae';
  			$chars[ 'ä' ] = 'ae';
  			$chars[ 'Ö' ] = 'Oe';
  			$chars[ 'ö' ] = 'oe';
  			$chars[ 'Ü' ] = 'Ue';
  			$chars[ 'ü' ] = 'ue';
  			$chars[ 'ß' ] = 'ss';
  		} elseif ( 'da_DK' === $locale ) {
  			$chars[ 'Æ' ] = 'Ae';
   			$chars[ 'æ' ] = 'ae';
  			$chars[ 'Ø' ] = 'Oe';
  			$chars[ 'ø' ] = 'oe';
  			$chars[ 'Å' ] = 'Aa';
  			$chars[ 'å' ] = 'aa';
  		} elseif ( 'ca' === $locale ) {
  			$chars[ 'l·l' ] = 'll';
  		} elseif ( 'sr_RS' === $locale || 'bs_BA' === $locale ) {
  			$chars[ 'Đ' ] = 'DJ';
  			$chars[ 'đ' ] = 'dj';
  		}
  		$string = strtr($string, $chars);
  	} else {
  		$chars = array();
  		// Assume ISO-8859-1 if not UTF-8
  		$chars['in'] = "\x80\x83\x8a\x8e\x9a\x9e"
  			."\x9f\xa2\xa5\xb5\xc0\xc1\xc2"
  			."\xc3\xc4\xc5\xc7\xc8\xc9\xca"
  			."\xcb\xcc\xcd\xce\xcf\xd1\xd2"
  			."\xd3\xd4\xd5\xd6\xd8\xd9\xda"
  			."\xdb\xdc\xdd\xe0\xe1\xe2\xe3"
  			."\xe4\xe5\xe7\xe8\xe9\xea\xeb"
  			."\xec\xed\xee\xef\xf1\xf2\xf3"
  			."\xf4\xf5\xf6\xf8\xf9\xfa\xfb"
  			."\xfc\xfd\xff";
  		$chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";
  		$string = strtr($string, $chars['in'], $chars['out']);
  		$double_chars = array();
  		$double_chars['in'] = array("\x8c", "\x9c", "\xc6", "\xd0", "\xde", "\xdf", "\xe6", "\xf0", "\xfe");
  		$double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
  		$string = str_replace($double_chars['in'], $double_chars['out'], $string);
  	}
  	return $string;
  }
  public function mbstring_binary_safe_encoding( $reset = false ) {
      static $encodings = array();
      static $overloaded = null;

      if ( is_null( $overloaded ) )
          $overloaded = function_exists( 'mb_internal_encoding' ) && ( ini_get( 'mbstring.func_overload' ) & 2 );

      if ( false === $overloaded )
          return;

      if ( ! $reset ) {
          $encoding = mb_internal_encoding();
          array_push( $encodings, $encoding );
          mb_internal_encoding( 'ISO-8859-1' );
      }

      if ( $reset && $encodings ) {
          $encoding = array_pop( $encodings );
          mb_internal_encoding( $encoding );
      }
  }
  public function reset_mbstring_encoding() {
      $this->mbstring_binary_safe_encoding( true );
  }
  /*
    Unique Multiple Array
  */
  public function uar($r,$k){
    $temp_array = array();
    foreach($r as &$v) {
      if(!isset($temp_array[$v[$k]])){
        $temp_array[$v[$k]] =& $v;
      }
    }
    $r = array_values($temp_array);
    return $r;
  }


}
